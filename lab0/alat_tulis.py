# Nama : Dantyo Budi Witjaksono Wijanarko
# NPM : 2106652051

# Membuat variabel barang
priceBulpen = 12000
pricePensil = 8000
priceCt = 9500

# Mencetak daftar dan harga masing-masing barang 
print("Toko ini menjual: ")
print("1. Pulpen " + str(priceBulpen) + "/pcs")
print("2. Pensil " + str(pricePensil) + "/pcs")
print("3. Correction Tape 8 M " + str(priceCt) + "/pcs")
print("Masukkan jumlah yang ingin anda beli")
jml_pulpen = int(input("Pulpen: "))
jml_pensil = int(input("Pensil: "))
jml_CT = int(input("Correction tape: "))

print(" ")

# membuat variabel baru untuk menyimpan jumlah masing-masing barang yang ingin dibeli
print("Ringkasan pembelian: ")
print(f"{jml_pulpen} pulpen x {priceBulpen}")
print(str(jml_pensil) + " pensil x " + str(pricePensil))
print(f"{jml_CT} corection tape x {priceCt}")

print(" ")

# mencetak total harga dengan  mengalikan jumlah dengan harga setiap barang
print(("Total harga: ") + str(jml_pulpen * priceBulpen) + str(jml_pensil * jml_pensil) + str(jml_CT * jml_CT))

# program selesai